module griddeploy.commands.common;

import griddeploy.apis;
import griddeploy.model;
import griddeploy.options;
import griddeploy.utils;

import std.process;
import std.path;
import std.string;

package string BuildInstallPath(Package spec, Options options) {
    string installPath = string.init;
    if (spec.Install.AllUsers) {
        version(Windows) {
            installPath = environment.get("SystemDrive");
            if (options.PlatformId.toLower() == "windows-x86") {
                installPath = buildNormalizedPath(installPath, "Program Files");
            }
            if (options.PlatformId.toLower() == "windows-x64") {
                installPath = buildNormalizedPath(installPath, spec.Meta.PlatformId.toLower() == "windows-x86" ? "Program Files (x86)" : "Program Files");
            }
            installPath = buildNormalizedPath(installPath, spec.Install.Path, options.ChannelId, spec.Meta.Version);
        }
    } else {
        version(Windows) {
            installPath = buildNormalizedPath(std.process.environment.get("USERPROFILE"), "AppData", "Local", spec.Install.Path, options.ChannelId, spec.Meta.Version);
        }
    }

    return installPath;
}

package void RunScripts(Package spec, string installPath, bool uninstall) {
    foreach(script; spec.Scripts) {
        if (script.Uninstall != uninstall) continue;
        string scriptPath = buildNormalizedPath(installPath, script.Path);

        if (script.Type == ScriptType.POWERSHELL) {
            version (Windows) {
                auto result = execute(["powershell.exe", "-File", scriptPath, script.Arguments]);
            } else {
                auto result = execute(["pwsh", "-File", scriptPath, script.Arguments]);
            }

            if (result.status != 0 && !script.ContinueOnFailure) {
                writeError(format("Script execution failed: %s", script.Path));
            }
        } else {
            auto result = execute([scriptPath, script.Arguments]);
            if (result.status != 0 && !script.ContinueOnFailure) {
                writeError(format("Script execution failed: %s", script.Path));
            }
        }
    }
}
