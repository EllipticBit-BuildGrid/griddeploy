module griddeploy.commands.install;

import griddeploy.apis;
import griddeploy.model;
import griddeploy.options;
import griddeploy.utils;
import griddeploy.commands.common;

import archive.core;
import archive.tar;
import archive.targz;
import zstd;

import std.file;
import std.range;
import std.path;
import std.process;
import std.stdio;
import std.string;
import std.uuid;
import std.zlib;

public void Install(Options options, string specFile, ubyte[] pack, ubyte[] uninstaller) {
    Package spec = new Package(specFile);
    if (spec is null) {
        writeError("No specification file.");
    }
    if (!matchPlatformId(spec.Meta.PlatformId.toLower(), options.PlatformId.toLower())) {
        writeError(format("No package available for system: %s", options.PlatformId.toLower()));
        return;
    }

    string installPath = BuildInstallPath(spec, options);
    writeln(installPath);
    string baseDirectory = dirName(installPath);
    
    PrepareInstallDirectory(spec, installPath);

    RunUninstall(spec, baseDirectory);

    ExtractPackage(spec, installPath, pack);

    CreateShortcuts(spec, installPath);

    RunScripts(spec, installPath, false);

    CreateUninstall(spec, options, uninstaller, baseDirectory, installPath);

    RunExecutable(spec, installPath);
}

private void PrepareInstallDirectory(Package spec, string installPath) {
    //Ensure install path exists
    mkdirRecurse(installPath);
}

private void ExtractPackage(Package spec, string installPath, ubyte[] pack) {
    string tempPath = buildNormalizedPath(tempDir(), randomUUID().toString());

    writeln("Unpacking package to directory: " ~ installPath);
    auto unpack = zstd.uncompress(pack);

    auto archive = new TarArchive(unpack);

    foreach(file; archive.files) {
        string filePath = buildNormalizedPath(installPath, cleanPath(file.path));
        mkdirRecurse(dirName(filePath));
        std.file.write(filePath, file.data);
    }
}

private void CreateShortcuts(Package spec, string installPath) {
version(Windows) {
    foreach(sc; spec.Shortcuts) {
        string shortcutPath = string.init;
        if (sc.Location == ShortcutLocation.Desktop) {
            shortcutPath = buildNormalizedPath(GetCurrentUserDesktopFolder(), sc.DisplayName ~ ".lnk");
        } else if (sc.Location == ShortcutLocation.StartMenu) {
            shortcutPath = buildNormalizedPath(spec.Install.AllUsers ? GetAllUserStartMenuFolder() : GetCurrentUserStartMenuFolder(), sc.Path, sc.DisplayName ~ ".lnk");
        }
        string targetPath = buildNormalizedPath(installPath, sc.File);
        SetFileShortcut(shortcutPath, targetPath, installPath, sc.Arguments, sc.Description, sc.IconIndex);
    }
}
}

private void RunExecutable(Package spec, string installPath) {
    if (spec.Runner is null) return;
    string exePath = buildNormalizedPath(installPath, spec.Runner.File);
    spawnProcess([exePath, spec.Runner.Arguments], null, Config.detached, dirName(exePath));
}

private void CreateUninstall(Package spec, Options options, ubyte[] uninstaller, string baseDirectory, string installPath) {
    //Write the spec to the base directory for the uninstall process.
    string specPath = buildNormalizedPath(baseDirectory, ".griddeploy");
    std.file.write(specPath, spec.toString());

    //Copy this excutable into the install directory for the uninstall process
    version(Windows) {
        std.file.write(buildNormalizedPath(baseDirectory, "uninstaller.exe"), uninstaller);
        SetRegistryUninstallData(spec.Meta.Id, options.ChannelId, spec.Meta.Title, spec.Meta.Version, spec.Meta.Owners.join("; "), baseDirectory, installPath, getDirectorySize(baseDirectory), spec.Install.AllUsers);
    } else {
        std.file.write(buildNormalizedPath(baseDirectory, "uninstaller"), uninstaller);
    }
}

private void RunUninstall(Package spec, string baseDirectory) {
    version(Windows) {
        string exePath = buildNormalizedPath(baseDirectory, "uninstaller.exe");
    } else {
        string exePath = buildNormalizedPath(baseDirectory, "uninstaller");
    }

    if (!exists(exePath)) return;

    spawnProcess([exePath]);
}
