module griddeploy.commands.uninstall;

import griddeploy.apis;
import griddeploy.utils;
import griddeploy.options;
import griddeploy.model;
import griddeploy.commands.common;

import std.file;
import std.path;

public void Uninstall(Options options) {
    auto deployFile = buildNormalizedPath(dirName(thisExePath()), "\\.griddeploy");
    if (!exists(deployFile)) {
        writeError("Unable to locate uninstallation script file.");
        return;
    }

    Package spec = new Package(parseFile(deployFile));

    string installPath = dirName(deployFile);
    string basePath = dirName(installPath);

    RunScripts(spec, installPath, true);

    DeleteShortcuts(spec);

    DeleteFiles(options, basePath, installPath);

    DeleteRegistryUninstall(spec, options);
}

public void DeleteFiles(Options options, string basePath, string installPath) {
    version(Windows) {
        rmdirRecurse(installPath);
        auto deployFile = buildNormalizedPath(basePath, "\\.griddeploy");
        remove(deployFile);
        auto oldSetupFile = buildNormalizedPath(basePath, "\\.delete");
        if (exists(oldSetupFile)) remove(oldSetupFile);
        rename(thisExePath(), oldSetupFile);
    }
    version(Posix) {
        rmdirRecurse(basePath);
    }
}

public void DeleteRegistryUninstall(Package spec, Options options) {
    version(Windows) {
        DeleteRegistryUninstallData(options.ProductId, options.ChannelId, spec.Install.AllUsers);
    }
}

public void DeleteShortcuts(Package spec) {
    foreach(sc; spec.Shortcuts) {
        string shortcutPath = string.init;
        if (sc.Location == ShortcutLocation.Desktop) {
            shortcutPath = buildNormalizedPath(GetCurrentUserDesktopFolder(), sc.DisplayName ~ ".lnk");
        } else if (sc.Location == ShortcutLocation.StartMenu) {
            shortcutPath = buildNormalizedPath(spec.Install.AllUsers ? GetAllUserStartMenuFolder() : GetCurrentUserStartMenuFolder(), sc.Path, sc.DisplayName ~ ".lnk");
        }
        remove(shortcutPath);
    }
}
