module griddeploy.model;

import griddeploy.utils;

import sdlite;

import std.algorithm;
import std.array;
import std.conv;
import std.datetime;
import std.file;
import std.path;
import std.stdio;
import std.string;
import std.typecons;

public enum ScriptType
{
    POWERSHELL,
    BATCH,
    SHELL,
}

public class Metadata
{
    public string Id;
    public string Title;
    public string Version;
    public string PlatformId;
    public string[] Authors;
    public string[] Owners;
    public string Copyright;
    public string IconPath;
    public string IconUrl;
    public string ProjectUrl;
    public string ReleaseNotesUrl;
    public string SupportUrl;
    public string LicenseUrl;
    public bool RequireLicenseAcceptance;
    public string Summary;
    public string Description;
    public string[] Tags;

    public this (SDLNode root, string baseDirectory, string overrideVersion, string overridePlatformId)
    {
        this.Id = root.expectNodeValue!string("id", 0);
        this.Title = root.expectNodeValue!string("title", 0);
        this.Version = overrideVersion == string.init ? root.expectNodeValue!string("version", 0) : overrideVersion;
        this.PlatformId = overridePlatformId == string.init ? root.expectNodeValue!string("platformId", 0) : overridePlatformId;

        string al = root.getNodeValue!string("authors", 0, null);
        if (al !is null)
        {
            string[] temp = string[].init;
            al.split(";").each!((s) { if (s.strip() != "") temp ~= s.strip; });
            this.Authors = temp;
        } else {
            this.Authors = null;
        }
        string ol = root.getNodeValue!string("owners", 0, null);
        if (ol !is null)
        {
            string[] temp = string[].init;
            ol.split(";").each!((s) { if (s.strip() != "") temp ~= s.strip; });
            this.Owners = temp;
        } else {
            this.Owners = null;
        }
        this.Copyright = root.getNodeValue!string("copyright", 0, null);
        string tip = root.getNodeValue!string("iconPath", 0, null);
        if (tip !is null) this.IconPath = buildNormalizedPath(baseDirectory, tip);
        this.IconUrl = root.getNodeValue!string("iconUrl", 0, null);
        this.ProjectUrl = root.getNodeValue!string("projectUrl", 0, null);
        this.ReleaseNotesUrl = root.getNodeValue!string("releaseNotesUrl", 0, null);
        this.SupportUrl = root.getNodeValue!string("supportUrl", 0, null);
        this.LicenseUrl = root.getNodeValue!string("licenseUrl", 0, null);
        this.RequireLicenseAcceptance = root.getNodeValue!bool("requireLicenseAcceptance", 0, false);
        this.Summary = root.getNodeValue!string("summary", 0, null);
        this.Description = root.getNodeValue!string("description", 0, null);
        string tl = root.getNodeValue!string("tags", 0, null);
        if (tl !is null)
        {
            string[] temp = string[].init;
            tl.split(";").each!((s) { if (s.strip() != "") temp ~= s.strip; });
            this.Tags = temp;
        } else {
            this.Tags = null;
        }
    }

    public SDLNode toNode() {
        SDLNode root = SDLNode("metadata");

        root.children ~= SDLNode("id", [SDLValue(this.Id)]);
        root.children ~= SDLNode("title", [SDLValue(this.Title)]);
        root.children ~= SDLNode("version", [SDLValue(this.Version)]);
        root.children ~= SDLNode("platformId", [SDLValue(this.PlatformId)]);

        if (this.Authors.length > 0) {
            string str = string.init;
            foreach(string s; this.Authors) {
                str ~= s ~ ";";
            }
            root.children ~= SDLNode("authors", [SDLValue(str)]);
        }
        if (this.Owners.length > 0) {
            string str = string.init;
            foreach(string s; this.Owners) {
                str ~= s ~ ";";
            }
            root.children ~= SDLNode("owners", [SDLValue(str)]);
        }
        if (this.Copyright !is null) root.children ~= SDLNode("copyright", [SDLValue(this.Copyright)]);
        if (this.IconUrl !is null) root.children ~= SDLNode("iconUrl", [SDLValue(this.IconUrl)]);
        if (this.ProjectUrl !is null) root.children ~= SDLNode("projectUrl", [SDLValue(this.ProjectUrl)]);
        if (this.ReleaseNotesUrl !is null) root.children ~= SDLNode("releaseNotesUrl", [SDLValue(this.ReleaseNotesUrl)]);
        if (this.SupportUrl !is null) root.children ~= SDLNode("supportUrl", [SDLValue(this.SupportUrl)]);
        if (this.LicenseUrl !is null) root.children ~= SDLNode("licenseUrl", [SDLValue(this.LicenseUrl)]);
        root.children ~= SDLNode("requireLicenseAcceptance", [SDLValue(cast(bool)this.RequireLicenseAcceptance)]);
        if (this.Summary !is null) root.children ~= SDLNode("summary", [SDLValue(this.Summary)]);
        if (this.Description !is null) root.children ~= SDLNode("description", [SDLValue(this.Description)]);
        if (this.Tags.length > 0) {
            string str = string.init;
            foreach(string s; this.Tags) {
                str ~= s ~ ";";
            }
            root.children ~= SDLNode("tags", [SDLValue(str)]);
        }

        return root;
    }
}

public class FileList
{
    public string[] Files;

    public this(SDLNode root, string baseDirectory)
    {
        auto path = buildNormalizedPath(baseDirectory == string.init ? getcwd() : baseDirectory, root.expectAttributeValue!string("path"));
        auto pattern = root.expectAttributeValue!string("pattern");

        string[] temp = pattern.split(";");

        DirEntry[] tfl = DirEntry[].init;
        foreach(string t; temp) {
            tfl ~= dirEntries(path, t.strip(), SpanMode.depth, true).array;
        }

        string[] inclusionList = GetInclusionList(root);
        string[] exclusionList = GetExclusionList(root);
        string[] fl = string[].init;
        foreach(DirEntry d; tfl) {
            if (exclusionList.any!(a => buildNormalizedPath(path, a) == d.name)()) {
                continue;
            }
            fl ~= d.name;
        }

        foreach(string e; inclusionList) {
            string ip = buildNormalizedPath(path, e); 
            if (exists(ip)) {
                fl ~= ip;
            }
        }

        this.Files = fl;
    }

    private string[] GetExclusionList(SDLNode root)
    {
        if (root.children.length == 0)
        {
            return null;
        }

        string[] tel = string[].init;

        foreach (SDLNode e; root.children)
        {
            if (e.name != "exclude") {
                continue;
            }

            tel ~= e.expectValue!string(0);
        }

        return tel;
    }

    private string[] GetInclusionList(SDLNode root)
    {
        if (root.children.length == 0)
        {
            return null;
        }

        string[] tel = string[].init;

        foreach (SDLNode e; root.children)
        {
            if (e.name != "include") {
                continue;
            }

            tel ~= e.expectValue!string(0);
        }

        return tel;
    }
}

public class Script
{
    public ScriptType Type;
    public string Path;
    public string Arguments;
    public bool ContinueOnFailure;
    public bool RequiresElevation;
    public int ExpectedExitCode;
    public bool Uninstall;

    public this (SDLNode root) {
        this.Type = to!ScriptType(root.expectAttributeValue!string("type"));
        this.Path = root.expectAttributeValue!string("path");
        this.Arguments = root.getAttributeValue!string("arguments", null);
        this.ContinueOnFailure = root.getAttributeValue!bool("continueOnFailure", false);
        this.RequiresElevation = root.getAttributeValue!bool("requiresElevation", false);
        this.ExpectedExitCode = root.getAttributeValue!int("expectedExitCode", 0);
        this.Uninstall = root.getAttributeValue!bool("uninstall", false);
    }

    public SDLNode toNode() {
        SDLNode root = SDLNode("script");
        root.attributes ~= SDLAttribute("type", SDLValue(to!string(this.Type)));
        root.attributes ~= SDLAttribute("path", SDLValue(this.Path));
        if (this.Arguments !is null) root.attributes ~= SDLAttribute("arguments", SDLValue(this.Arguments));
        root.attributes ~= SDLAttribute("continueOnFailure", SDLValue(cast(bool)this.ContinueOnFailure));
        root.attributes ~= SDLAttribute("expectedExitCode", SDLValue(cast(int)this.ExpectedExitCode));
        root.attributes ~= SDLAttribute("requiresElevation", SDLValue(cast(bool)this.RequiresElevation));
        root.attributes ~= SDLAttribute("uninstall", SDLValue(cast(bool)this.Uninstall));

        return root;
    }
}

public class Installation {
    public string DisplayName;
    public string Path;
    public bool AllUsers;

    public this(SDLNode root) {
        this.DisplayName = root.expectAttributeValue!string("displayName");
        this.Path = root.getAttributeValue!string("path", string.init);
        this.AllUsers = root.getAttributeValue!bool("allUsers", false);
    }

    public SDLNode toNode() {
        SDLNode root = SDLNode("install");
        root.attributes ~= SDLAttribute("displayName", SDLValue(this.DisplayName));
        if (this.Path != string.init) root.attributes ~= SDLAttribute("path", SDLValue(this.Path));
        if (this.AllUsers == true) root.attributes ~= SDLAttribute("allUsers", SDLValue(true));
        return root;
    }
}

public enum ShortcutLocation {
    Desktop,
    StartMenu
}

public class Shortcut {
    public string DisplayName;
    public string File;
    public string Options;
    public ShortcutLocation Location;
    public string Path;
    public string Description;
    public string Arguments;
    public int IconIndex;

    public this(SDLNode root) {
        this.DisplayName = root.expectAttributeValue!string("displayName");
        this.File = root.expectAttributeValue!string("file");
        this.Options = root.getAttributeValue!string("options", string.init);
        this.Location = to!ShortcutLocation(root.expectAttributeValue!string("location"));
        this.Path = root.getAttributeValue!string("path", string.init);
        this.Description = root.getAttributeValue!string("description", null);
        this.Arguments = root.getAttributeValue!string("arguments", null);
        this.IconIndex = root.getAttributeValue!int("iconIndex", int.min);
    }

    public SDLNode toNode() {
        SDLNode root = SDLNode("shortcut");
        root.attributes ~= SDLAttribute("displayName", SDLValue(this.DisplayName));
        root.attributes ~= SDLAttribute("file", SDLValue(this.File));
        if (this.Options != string.init) root.attributes ~= SDLAttribute("options", SDLValue(this.Options));
        root.attributes ~= SDLAttribute("location", SDLValue(to!string(this.Location)));
        if (this.Path != string.init) root.attributes ~= SDLAttribute("path", SDLValue(this.Path));
        if (this.Description !is null) root.attributes ~= SDLAttribute("description", SDLValue(this.Description));
        if (this.Arguments !is null) root.attributes ~= SDLAttribute("arguments", SDLValue(this.Arguments));
        if (this.IconIndex != int.min) root.attributes ~= SDLAttribute("iconIndex", SDLValue(cast(int)this.IconIndex));
        return root;
    }
}

public class Run {
    public string File;
    public string Arguments;

    public this(SDLNode root) {
        this.File = root.expectAttributeValue!string("file");
        this.Arguments = root.getAttributeValue!string("arguments", null);
    }

    public SDLNode toNode() {
        SDLNode root = SDLNode("run");
        root.attributes ~= SDLAttribute("file", SDLValue(this.File));
        if (this.Arguments !is null) root.attributes ~= SDLAttribute("arguments", SDLValue(this.Arguments));
        return root;
    }
}

public class Package
{
    public Metadata Meta;
    public FileList[] Files;
    public Script[] Scripts;
    public Installation Install;
    public Shortcut[] Shortcuts;
    public Run Runner;

    public this(string sdlDocument, string baseDirectory = string.init, string overrideVersion = string.init, string overridePlatformId = string.init) {
        SDLNode[] docNodes;
        parseSDLDocument!((n) { docNodes ~= n; })(sdlDocument, "install.sdl");

        this(docNodes[0], baseDirectory, overrideVersion, overridePlatformId);
    }

    public this(SDLNode root, string baseDirectory = string.init, string overrideVersion = string.init, string overridePlatformId = string.init) {
        foreach(SDLNode t; root.children) {
            if (t.name.toUpper() == "metadata".toUpper()) {
                Meta = new Metadata(t, baseDirectory, overrideVersion, overridePlatformId);
            }
            else if (t.name.toUpper() == "files".toUpper()) {
                Files ~= new FileList(t, baseDirectory);
            }
            else if (t.name.toUpper() == "script".toUpper()) {
                Scripts ~= new Script(t);
            }
            else if (t.name.toUpper() == "install".toUpper()) {
                Install = new Installation(t);
            }
            else if (t.name.toUpper() == "shortcut".toUpper()) {
                Shortcuts ~= new Shortcut(t);
            }
            else if (t.name.toUpper() == "run".toUpper()) {
                Runner = new Run(t);
            }
        }
    }

    public override string toString() {
        SDLNode root = SDLNode();

        root.children ~= Meta.toNode();
        if (this.Install !is null) root.children ~= this.Install.toNode();
        if (this.Runner !is null) root.children ~= this.Runner.toNode();
        foreach(Shortcut t; this.Shortcuts) {
            root.children ~= t.toNode();
        }
        foreach(Script t; this.Scripts) {
            root.children ~= t.toNode();
        }

        auto app = appender!string();
        app.generateSDLang(root);
        return app.data;
    }
}

public SDLNode parseFile(string path) {
	string sdl = stripBOM(readText(path));

	SDLNode[] docNodes;
	parseSDLDocument!((n) { docNodes ~= n; })(sdl, baseName(path));

	return docNodes[0];
}

/// Internal methods for parsing SDL with the SDLite library.

private Nullable!SDLNode getNode(SDLNode node, string nodeName) {
	foreach(n; node.children) {
		if (n.qualifiedName == nodeName) return Nullable!SDLNode(n);
	}

	return Nullable!SDLNode();
}

private T expectNodeValue(T)(SDLNode node, string nodeName, int index)
	if (is(T == string) ||
		is(T == immutable(ubyte)[]) ||
		is(T == int) ||
		is(T == long) ||
		is(T == long[2]) ||
		is(T == float) ||
		is(T == double) ||
		is(T == bool) ||
		is(T == SysTime) ||
		is(T == Date) ||
		is(T == Duration))
{
	Nullable!SDLNode tn = getNode(node, nodeName);
	if (tn.isNull) throw new Exception("Node with name '" ~ nodeName ~ "' not found.");

    SDLNode n = tn.get();
	if (index >= n.values.length) throw new Exception("Required Value not found at index: " ~ to!string(index));

	return n.values[index].value!T();
}

private T getNodeValue(T)(SDLNode node, string nodeName, int index, T defaultValue)
	if (is(T == string) ||
		is(T == immutable(ubyte)[]) ||
		is(T == int) ||
		is(T == long) ||
		is(T == long[2]) ||
		is(T == float) ||
		is(T == double) ||
		is(T == bool) ||
		is(T == SysTime) ||
		is(T == Date) ||
		is(T == Duration))
{
	auto tn = getNode(node, nodeName);
	if (tn.isNull || index >= tn.get().values.length) return defaultValue;

	return tn.get().values[index].value!T();
}

private T expectAttributeValue(T)(SDLNode node, string name)
	if (is(T == string) ||
		is(T == immutable(ubyte)[]) ||
		is(T == int) ||
		is(T == long) ||
		is(T == long[2]) ||
		is(T == float) ||
		is(T == double) ||
		is(T == bool) ||
		is(T == SysTime) ||
		is(T == Date) ||
		is(T == Duration))
{
	auto attr = node.getAttribute(name, SDLValue.null_);
	if (attr == SDLValue.null_) {
		throw new Exception("Required Attribute not found: " ~ name);
	}

	return attr.value!T;
}

private T getAttributeValue(T)(SDLNode node, string name, T defaultValue)
	if (is(T == string) ||
		is(T == immutable(ubyte)[]) ||
		is(T == int) ||
		is(T == long) ||
		is(T == long[2]) ||
		is(T == float) ||
		is(T == double) ||
		is(T == bool) ||
		is(T == SysTime) ||
		is(T == Date) ||
		is(T == Duration))
{
	return node.getAttribute(name, SDLValue(defaultValue)).value!T;
}

private T expectValue(T)(SDLNode node, int index)
	if (is(T == string) ||
		is(T == immutable(ubyte)[]) ||
		is(T == int) ||
		is(T == long) ||
		is(T == long[2]) ||
		is(T == float) ||
		is(T == double) ||
		is(T == bool) ||
		is(T == SysTime) ||
		is(T == Date) ||
		is(T == Duration))
{
	if (index >= node.values.length) {
		throw new Exception("No Value found at specified index: " ~ to!string(index));
	}

	return node.values[index].value!T();
}
