module griddeploy.options;

import griddeploy.apis;

import std.array;
import std.algorithm.searching;
import std.conv;
import std.file;
import std.path;
import std.process;
import std.stdio;
import std.string;

public class Options
{
    public string TenantId;
    public string ProductId;
    public string ChannelId;
    public string PlatformId;
    public string ApiKey;

    private this(string tenantId, string productId, string channelId, string platformId, string apiKey) {
        this.TenantId = tenantId;
        this.ProductId = productId;
        this.ChannelId = channelId;
        this.PlatformId = platformId;
        this.ApiKey = apiKey;
    }
}

public Options GetOptions(string tenantId, string productId, string channelId, string apiKey)
{
    string platformId = GetSystemArch();

    return new Options(tenantId, productId, channelId, platformId, apiKey);
}

public Options GetOptionsFromArgs(string[] args, string tenantId, string productId, string channelId)
{
    string platformId = GetSystemArch();
    string apiKey = string.init;

    if (args.length == 0) { }
    else if (args.length == 2) {
        channelId = args[0];
        apiKey = args[1];
    }
    else {
        writeln("ERROR: Invalid arguments specfied.");
        return null;
    }

    return new Options(tenantId, productId, channelId, platformId, apiKey);
}

private string GetSystemArch() {
    version(Windows) {
        string arch = GetRegistryStringValue("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment", "PROCESSOR_ARCHITECTURE", true);
        if (arch.strip().toLower() == "x86".toLower()) return "windows-x86";
        else if (arch.strip().toLower() == "AMD64".toLower()) return "windows-x64";
        else return "windows-any";
    }
    version(linux) {
        auto uname = executeShell("uname -m"); 
        string arch = uname.output.strip();
        if (arch.strip().toLower() == "i386".toLower() || arch.toLower() == "i586".toLower() || arch.toLower() == "i686".toLower()) return "linux-x86";
        else if (arch.strip().toLower() == "x86_64".toLower()) return "linux-x64";
        else return "linux-any";
    }
/*
    version(OSX) {
        return "macos-x64";
    }
*/
    else {
        static assert("Unsupported platform." !is null);
    }
}
