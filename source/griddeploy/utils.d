module griddeploy.utils;

import std.conv;
import std.file;
import std.process;
import std.stdio;
import std.string;

public __gshared int errorCount;

public void writeError(string message) {
    errorCount++;
    writefln("ERROR: %s", message);
}

public int getDirectorySize(string path) {
    ulong totalSize = 0;
    auto files = dirEntries(path, SpanMode.depth);
    foreach(f; files) {
        totalSize += getSize(f.name);
    }
    return to!int(totalSize / 1024);
}

public bool matchPlatformId(string packagePlatformId, string systemPlatformId) {
    if (systemPlatformId.toLower() == "windows-any".toLower() &&
       (packagePlatformId.toLower() == "windows-any".toLower() ||
        packagePlatformId.toLower() == "any".toLower())) return true;
    if (systemPlatformId.toLower() == "windows-x86".toLower() &&
       (packagePlatformId.toLower() == "windows-x86".toLower() ||
        packagePlatformId.toLower() == "windows-any".toLower() ||
        packagePlatformId.toLower() == "any".toLower())) return true;
    if (systemPlatformId.toLower() == "windows-x64".toLower() &&
       (packagePlatformId.toLower() == "windows-x64".toLower() ||
        packagePlatformId.toLower() == "windows-x86".toLower() ||
        packagePlatformId.toLower() == "windows-any".toLower() ||
        packagePlatformId.toLower() == "any".toLower())) return true;

    if (systemPlatformId.toLower() == "linux-any".toLower() &&
       (packagePlatformId.toLower() == "linux-any".toLower() ||
        packagePlatformId.toLower() == "any".toLower())) return true;
    if (systemPlatformId.toLower() == "linux-x86".toLower() &&
       (packagePlatformId.toLower() == "linux-x86".toLower() ||
        packagePlatformId.toLower() == "linux-any".toLower() ||
        packagePlatformId.toLower() == "any".toLower())) return true;
    if (systemPlatformId.toLower() == "linux-x64".toLower() &&
       (packagePlatformId.toLower() == "linux-x64".toLower() ||
        packagePlatformId.toLower() == "linux-x86".toLower() ||
        packagePlatformId.toLower() == "linux-any".toLower() ||
        packagePlatformId.toLower() == "any".toLower())) return true;
    if (systemPlatformId.toLower() == "macos-x64".toLower() &&
       (packagePlatformId.toLower() == "macos-x64".toLower() ||
        packagePlatformId.toLower() == "macos-any".toLower() ||
        packagePlatformId.toLower() == "any".toLower())) return true;
    return false;
}

public string cleanPath(string path) {
    return path.strip("\\").strip("/");
}

public @trusted string stripBOM(string str) {
    import std.encoding : getBOM;
    return str[getBOM(str.representation).sequence.length..$];
}
