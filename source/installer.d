version (Disable):

import griddeploy.model;
import griddeploy.options;
import griddeploy.utils;

import griddeploy.commands.install;
import griddeploy.commands.uninstall;

public immutable string DefaultTenantId  = "<TenantId>";
public immutable string DefaultProductId = "<ProductId>";
public immutable string DefaultChannelId = "<ChannelId>";

version(Installer) {
	public immutable string SpecData = import("spec.sdl");
	public immutable ubyte[] Uninstaller = import("uninstaller.bin");
	public immutable ubyte[] Package = import("package.bin");
}

int main(string[] args)
{
	Options opts = GetOptionsFromArgs(args[1..$], DefaultTenantId, DefaultProductId, DefaultChannelId);
	if (opts is null) {
		return 1;
	}

	version(Installer) {
		Install(opts, SpecData, Uninstaller, Package);
	}

	version (Uninstaller) {
		Uninstall(opts);
	}

	return errorCount;
}
